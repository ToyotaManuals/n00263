<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<link rel="stylesheet" type="text/css" href="../../../system/css/global.css">
<link rel="stylesheet" type="text/css" href="../css/contents.css">
<link rel="stylesheet" type="text/css" href="../../../system/css/facebox.css">
<link rel="stylesheet" type="text/css" href="../css/print.css" id="print_css">
<script type="text/javascript" src="../../../system/js/util.js?cntl=Contents" charset="UTF-8"></script>
<script type="text/javascript" src="../../../system/js/dict_const.js" charset="UTF-8"></script>
<script type="text/javascript" src="../../../system/js/dict_message.js" charset="UTF-8"></script>
<script type="text/javascript" src="../../../system/js/use.js" charset="UTF-8"></script>
<script type="text/javascript" src="../../../system/js/contents.js" charset="UTF-8"></script>
<title>Toyota Service Information</title>
</head>
<body>
<div id="wrapper">
<div id="header"></div>
<div id="body">
<div id="contents">
<div id="contentsBody" class="fontEn">
<div class="globalInfo">
<span class="globalPubNo">RM1440E</span>
<span class="globalServcat">_51</span>
<span class="globalServcatName">Engine / Hybrid System</span>
<span class="globalSection">_056124</span>
<span class="globalSectionName">1GR-FE ENGINE CONTROL</span>
<span class="globalTitle">_0273876</span>
<span class="globalTitleName">SFI SYSTEM</span>
<span class="globalCategory">C</span>
</div>
<h1>1GR-FE ENGINE CONTROL&nbsp;&nbsp;SFI SYSTEM&nbsp;&nbsp;P2102&nbsp;&nbsp;Throttle Actuator Control Motor Circuit Low&nbsp;&nbsp;P2103&nbsp;&nbsp;Throttle Actuator Control Motor Circuit High&nbsp;&nbsp;</h1>
<br>
<div id="RM100000000XNSO_01" class="category no03">
<h2>DESCRIPTION</h2>
<div class="content5">
<p>The throttle actuator is operated by the ECM and opens and closes the throttle valve using gears.
</p>
<p>The opening angle of the throttle valve is detected by the throttle position sensor, which is mounted on the throttle body with motor assembly. The throttle position sensor provides feedback to the ECM. This feedback allows the ECM to appropriately control the throttle actuator and monitor the throttle opening angle as the ECM responds to driver inputs.
</p>
<dl class="atten4">
<dt class="atten4">HINT:</dt>
<dd class="atten4">
<p>This ETCS (Electronic Throttle Control System) does not use a throttle cable.
</p>
</dd>
<dd class="atten4"><table summary="">
<colgroup>
<col style="width:12%">
<col style="width:44%">
<col style="width:43%">
</colgroup>
<thead>
<tr>
<th class="alcenter">DTC No.
</th>
<th class="alcenter">DTC Detection Condition
</th>
<th class="alcenter">Trouble Area
</th>
</tr>
</thead>
<tbody>
<tr>
<td class="alcenter">P2102
</td>
<td>Conditions (a) and (b) are met for 2.0 seconds (1 trip detection logic):
<br>
(a) Throttle actuator duty ratio is 80% or more.
<br>
(b) Throttle actuator current is below 0.5 A.
</td>
<td><div class="list1">
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>Open in throttle actuator circuit
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>Throttle actuator
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>ECM
</p>
</div>
</div>
</div>
<br>
</td>
</tr>
<tr>
<td class="alcenter">P2103
</td>
<td>Either condition is met (1 trip detection logic):
<br>
<div class="list1">
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>There is a hybrid IC diagnosis signal failure.
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>There is a hybrid IC current limiter port failure.
</p>
</div>
</div>
</div>
<br>
</td>
<td><div class="list1">
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>Short in throttle actuator circuit
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>Throttle actuator
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>Throttle valve
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>Throttle body with motor assembly
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>ECM
</p>
</div>
</div>
</div>
<br>
</td>
</tr>
</tbody>
</table>
<br>
</dd>
</dl>
<br>
</div>
</div>
<div id="RM100000000XNSO_02" class="category no03">
<h2>MONITOR DESCRIPTION</h2>
<div class="content5">
<p>The ECM monitors the electrical current through the electronic actuator, and detects malfunctions and open circuits in the throttle actuator based on this value. If the current is outside the standard range, the ECM determines that there is a malfunction in the throttle actuator. In addition, if the throttle valve does not function properly (for example, if it is stuck on), the ECM determines that there is a malfunction. The ECM then illuminates the MIL and stores a DTC.
</p>
<p>Example:
</p>
<p>When the electrical current is less than 0.5 A and the throttle actuator duty ratio is 80% or more, the ECM interprets this as the current being outside the standard range, illuminates the MIL and stores DTC P2102.
</p>
<p>If the malfunction is not repaired successfully, the DTC is stored when the engine is quickly revved to a high rpm several times after the engine is started and has idled for 5 seconds.
</p>
</div>
</div>
<div id="RM100000000XNSO_06" class="category no03">
<h2>FAIL-SAFE</h2>
<div class="content5">
<p>When either of these DTCs or other DTCs relating to ETCS (Electronic Throttle Control System) malfunctions are set, the ECM enters fail-safe mode. During fail-safe mode, the ECM cuts the current to the throttle actuator, and the throttle valve is returned to a 7° opening angle by the return spring. The ECM then adjusts the engine output by controlling the fuel injection (intermittent fuel-cut) and ignition timing in accordance with the accelerator pedal position to allow the vehicle to continue at a minimal speed. If the accelerator pedal is depressed firmly and gently, the vehicle can be driven slowly.
</p>
<p>The ECM continues operating in fail-safe mode until a pass condition is detected and the ignition switch is turned off.
</p>
</div>
</div>
<div id="RM100000000XNSO_07" class="category no03">
<h2>WIRING DIAGRAM</h2>
<div class="content5">
<div class="figure">
<div class="cPattern">
<div class="graphic">
<img src="../img/png/A227104E01.png" alt="A227104E01" title="A227104E01">
<div class="indicateinfo">
<span class="caption">
<span class="points">1.381,3.265 1.678,3.426</span>
<span class="captionSize">0.298,0.161</span>
<span class="fontsize">10</span>
<span class="captionText">B13</span>
</span>
<span class="caption">
<span class="points">1.388,3.426 3.610,3.619</span>
<span class="captionSize">2.222,0.193</span>
<span class="fontsize">10</span>
<span class="captionText">Throttle Body with Motor Assembly</span>
</span>
<span class="caption">
<span class="points">0.927,4.11 1.389,4.313</span>
<span class="captionSize">0.461,0.202</span>
<span class="fontsize">10</span>
<span class="captionText">Battery</span>
</span>
<span class="caption">
<span class="points">4.112,1.81 4.685,1.989</span>
<span class="captionSize">0.573,0.179</span>
<span class="fontsize">10</span>
<span class="captionText">Shielded</span>
</span>
<span class="caption">
<span class="points">2.89,1.132 3.296,1.315</span>
<span class="captionSize">0.406,0.183</span>
<span class="fontsize">10</span>
<span class="captionText">ETCS</span>
</span>
<span class="caption">
<span class="points">5.536,0.803 5.743,0.954</span>
<span class="captionSize">0.207,0.151</span>
<span class="fontsize">10</span>
<span class="captionText">23</span>
</span>
<span class="caption">
<span class="points">5.496,0.987 5.767,1.133</span>
<span class="captionSize">0.271,0.146</span>
<span class="fontsize">10</span>
<span class="captionText">B38</span>
</span>
<span class="caption">
<span class="points">5.812,0.897 6.153,1.062</span>
<span class="captionSize">0.341,0.165</span>
<span class="fontsize">10</span>
<span class="captionText">+BM</span>
</span>
<span class="caption">
<span class="points">5.537,1.96 5.784,2.097</span>
<span class="captionSize">0.247,0.137</span>
<span class="fontsize">10</span>
<span class="captionText">20</span>
</span>
<span class="caption">
<span class="points">5.5,2.154 5.785,2.347</span>
<span class="captionSize">0.286,0.193</span>
<span class="fontsize">10</span>
<span class="captionText">B39</span>
</span>
<span class="caption">
<span class="points">5.539,2.587 5.738,2.751</span>
<span class="captionSize">0.199,0.163</span>
<span class="fontsize">10</span>
<span class="captionText">29</span>
</span>
<span class="caption">
<span class="points">5.501,2.768 5.784,2.919</span>
<span class="captionSize">0.283,0.151</span>
<span class="fontsize">10</span>
<span class="captionText">B38</span>
</span>
<span class="caption">
<span class="points">2.767,2.061 3.000,2.217</span>
<span class="captionSize">0.232,0.156</span>
<span class="fontsize">10</span>
<span class="captionText">M+</span>
</span>
<span class="caption">
<span class="points">2.781,2.677 2.985,2.824</span>
<span class="captionSize">0.204,0.147</span>
<span class="fontsize">10</span>
<span class="captionText">M-</span>
</span>
<span class="caption">
<span class="points">3.066,2.054 3.183,2.210</span>
<span class="captionSize">0.117,0.156</span>
<span class="fontsize">10</span>
<span class="captionText">2</span>
</span>
<span class="caption">
<span class="points">3.065,2.671 3.174,2.840</span>
<span class="captionSize">0.110,0.170</span>
<span class="fontsize">10</span>
<span class="captionText">1</span>
</span>
<span class="caption">
<span class="points">5.535,3.186 5.728,3.342</span>
<span class="captionSize">0.193,0.156</span>
<span class="fontsize">10</span>
<span class="captionText">30</span>
</span>
<span class="caption">
<span class="points">5.501,3.368 5.794,3.523</span>
<span class="captionSize">0.293,0.156</span>
<span class="fontsize">10</span>
<span class="captionText">B38</span>
</span>
<span class="caption">
<span class="points">5.532,3.775 5.725,3.945</span>
<span class="captionSize">0.193,0.170</span>
<span class="fontsize">10</span>
<span class="captionText">10</span>
</span>
<span class="caption">
<span class="points">5.501,3.955 5.795,4.101</span>
<span class="captionSize">0.294,0.146</span>
<span class="fontsize">10</span>
<span class="captionText">B39</span>
</span>
<span class="caption">
<span class="points">5.531,4.368 5.724,4.556</span>
<span class="captionSize">0.193,0.188</span>
<span class="fontsize">10</span>
<span class="captionText">12</span>
</span>
<span class="caption">
<span class="points">5.5,4.548 5.803,4.689</span>
<span class="captionSize">0.303,0.142</span>
<span class="fontsize">10</span>
<span class="captionText">B38</span>
</span>
<span class="caption">
<span class="points">5.816,2.056 6.046,2.235</span>
<span class="captionSize">0.230,0.179</span>
<span class="fontsize">10</span>
<span class="captionText">M+</span>
</span>
<span class="caption">
<span class="points">5.812,2.672 6.005,2.819</span>
<span class="captionSize">0.193,0.147</span>
<span class="fontsize">10</span>
<span class="captionText">M-</span>
</span>
<span class="caption">
<span class="points">5.812,3.354 6.200,3.524</span>
<span class="captionSize">0.387,0.170</span>
<span class="fontsize">10</span>
<span class="captionText">GE01</span>
</span>
<span class="caption">
<span class="points">5.815,3.859 6.198,4.002</span>
<span class="captionSize">0.383,0.144</span>
<span class="fontsize">10</span>
<span class="captionText">ME01</span>
</span>
<span class="caption">
<span class="points">5.815,4.54 6.010,4.712</span>
<span class="captionSize">0.195,0.172</span>
<span class="fontsize">10</span>
<span class="captionText">E1</span>
</span>
<span class="caption">
<span class="points">6.13,5.11 6.480,5.275</span>
<span class="captionSize">0.350,0.165</span>
<span class="fontsize">10</span>
<span class="captionText">ECM</span>
</span>
</div>
</div>
</div>
</div>
<br>
</div>
</div>
<div id="RM100000000XNSO_08" class="category no10">
<h2>CAUTION / NOTICE / HINT</h2>
<div class="content5">
<dl class="atten4">
<dt class="atten4">HINT:</dt>
<dd class="atten4"><div class="list1">
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>Read freeze frame data using the intelligent tester. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, if the air-fuel ratio was lean or rich, and other data from the time the malfunction occurred.
</p>
</div>
</div>
</div>
<br>
</dd>
<dd class="atten4"><div class="list1">
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>The throttle actuator current (Throttle Motor Current) and the throttle actuator duty ratio (Throttle Motor Duty (Open) / Throttle Motor Duty (Close)) can be read using the intelligent tester. However, the ECM shuts off the throttle actuator current when the ETCS malfunctions.
</p>
</div>
</div>
</div>
<br>
</dd>
</dl>
<br>
</div>
</div>
<div id="RM100000000XNSO_09" class="category no01">
<h2>PROCEDURE</h2>
<div class="testgrp" id="RM100000000XNSO_09_0001">
<div class="testtitle"><span class="titleText">1.INSPECT THROTTLE BODY WITH MOTOR ASSEMBLY (RESISTANCE OF THROTTLE ACTUATOR)</span></div>
<div class="content6">
<div class="test1">
<div class="test1Item">
<div class="test1Head">a.</div>
<div class="test1Body">
<p>Inspection the throttle body with motor assembly (See page <a class="mlink" href="javascript:void(0)">Engine / Hybrid System&gt;1GR-FE ENGINE CONTROL&gt;THROTTLE BODY&gt;INSPECTION<span class="invisible">200908,999999,_51,_056124,_0273863,RM100000000XNPX_01_0001,</span></a>).
</p>
</div>
</div>
</div>
<br>
</div>
<br>
<div class="judgeValueNext"><p>OK
</p></div>
<div class="nextAction"><span class="titleText">2.CHECK HARNESS AND CONNECTOR (THROTTLE BODY WITH MOTOR ASSEMBLY - ECM)</span></div>
<div class="judgeValueEnd"><p>NG
</p></div>
<div class="endActBox">
<div class="endAction"><span class="titleText">REPLACE THROTTLE BODY WITH MOTOR ASSEMBLY</span></div>
<div class="content6">
<p>(See page<a class="mlink" href="javascript:void(0)">Engine / Hybrid System&gt;1GR-FE ENGINE CONTROL&gt;THROTTLE BODY&gt;REMOVAL<span class="invisible">200908,201308,_51,_056124,_0273863,RM100000000XNQ0,</span><span class="invisible">201308,999999,_51,_056124,_0273863,RM100000000XNQ1,</span></a>)
</p>
</div>
<br>
<div class="floatClear"></div>
</div>
<div class="floatClear"></div>
</div>
<div class="testgrp" id="RM100000000XNSO_09_0002">
<div class="testtitle"><span class="titleText">2.CHECK HARNESS AND CONNECTOR (THROTTLE BODY WITH MOTOR ASSEMBLY - ECM)</span></div>
<div class="content6">
<div class="test1">
<div class="test1Item">
<div class="test1Head">a.</div>
<div class="test1Body">
<p>Disconnect the throttle body with motor assembly connector.
</p>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">b.</div>
<div class="test1Body">
<p>Disconnect the ECM connector.
</p>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">c.</div>
<div class="test1Body">
<p>Measure the resistance according to the value(s) in the table below.
</p>
<dl class="spec">
<dt class="spec">Standard Resistance:</dt>
<dd class="spec"><table summary="" class="half">
<colgroup>
<col style="width:33%">
<col style="width:33%">
<col style="width:33%">
</colgroup>
<thead>
<tr>
<th class="alcenter">Tester Connection
</th>
<th class="alcenter">Condition
</th>
<th class="alcenter">Specified Condition
</th>
</tr>
</thead>
<tbody>
<tr>
<td class="alcenter">B13-2 (M+) - B39-20 (M+)
</td>
<td class="alcenter">Always
</td>
<td class="alcenter">Below 1 Ω
</td>
</tr>
<tr>
<td class="alcenter">B13-1 (M-) - B38-29 (M-)
</td>
<td class="alcenter">Always
</td>
<td class="alcenter">Below 1 Ω
</td>
</tr>
<tr>
<td class="alcenter">B13-2 (M+) or B39-20 (M+) - Body ground
</td>
<td class="alcenter">Always
</td>
<td class="alcenter">10 kΩ or higher
</td>
</tr>
<tr>
<td class="alcenter">B13-1 (M-) or B38-29 (M-) - Body ground
</td>
<td class="alcenter">Always
</td>
<td class="alcenter">10 kΩ or higher
</td>
</tr>
</tbody>
</table>
<br>
</dd>
</dl>
<br>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">d.</div>
<div class="test1Body">
<p>Reconnect the throttle body with motor assembly connector.
</p>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">e.</div>
<div class="test1Body">
<p>Reconnect the ECM connector.
</p>
</div>
</div>
</div>
<br>
</div>
<br>
<div class="judgeValueNext"><p>OK
</p></div>
<div class="nextAction"><span class="titleText">3.INSPECT THROTTLE BODY WITH MOTOR ASSEMBLY</span></div>
<div class="judgeValueEnd"><p>NG
</p></div>
<div class="endActBox">
<div class="endAction"><span class="titleText">REPAIR OR REPLACE HARNESS OR CONNECTOR</span></div>
<div class="content6">
</div>
<br>
<div class="floatClear"></div>
</div>
<div class="floatClear"></div>
</div>
<div class="testgrp" id="RM100000000XNSO_09_0003">
<div class="testtitle"><span class="titleText">3.INSPECT THROTTLE BODY WITH MOTOR ASSEMBLY</span></div>
<div class="content6">
<div class="test1">
<div class="test1Item">
<div class="test1Head">a.</div>
<div class="test1Body">
<p>Check for foreign objects between the throttle valve and housing.
</p>
<dl class="spec">
<dt class="spec">OK:</dt>
<dd class="spec"><p>No foreign objects between throttle valve and housing.
</p>
</dd>
</dl>
<br>
</div>
</div>
</div>
<br>
</div>
<br>
<div class="judgeValueNext"><p>OK
</p></div>
<div class="nextAction"><span class="titleText">4.INSPECT THROTTLE VALVE</span></div>
<div class="judgeValueEnd"><p>NG
</p></div>
<div class="endActBox">
<div class="endAction"><span class="titleText">REMOVE FOREIGN OBJECT AND CLEAN THROTTLE BODY WITH MOTOR ASSEMBLY</span></div>
<div class="content6">
<p>(See page<a class="mlink" href="javascript:void(0)">Engine / Hybrid System&gt;1GR-FE ENGINE CONTROL&gt;THROTTLE BODY&gt;REMOVAL<span class="invisible">200908,201308,_51,_056124,_0273863,RM100000000XNQ0,</span><span class="invisible">201308,999999,_51,_056124,_0273863,RM100000000XNQ1,</span></a>)
</p>
</div>
<br>
<div class="floatClear"></div>
</div>
<div class="floatClear"></div>
</div>
<div class="testgrp" id="RM100000000XNSO_09_0004">
<div class="testtitle"><span class="titleText">4.INSPECT THROTTLE VALVE</span></div>
<div class="content6">
<div class="test1">
<div class="test1Item">
<div class="test1Head">a.</div>
<div class="test1Body">
<p>Check if the throttle valve opens and closes smoothly.
</p>
<dl class="spec">
<dt class="spec">OK:</dt>
<dd class="spec"><p>Throttle valve opens and closes smoothly.
</p>
</dd>
</dl>
<br>
</div>
</div>
</div>
<br>
</div>
<br>
<div class="judgeValueEnd"><p>OK
</p></div>
<div class="endActBox">
<div class="endAction"><span class="titleText">REPLACE ECM</span></div>
<div class="content6">
<p>(See page<a class="mlink" href="javascript:void(0)">Engine / Hybrid System&gt;1GR-FE ENGINE CONTROL&gt;ECM&gt;REMOVAL<span class="invisible">200908,999999,_51,_056124,_0273864,RM100000000XNQ5,</span></a>)
</p>
</div>
<br>
<div class="floatClear"></div>
</div>
<div class="judgeValueEnd"><p>NG
</p></div>
<div class="endActBox">
<div class="endAction"><span class="titleText">REPLACE THROTTLE BODY WITH MOTOR ASSEMBLY</span></div>
<div class="content6">
<p>(See page<a class="mlink" href="javascript:void(0)">Engine / Hybrid System&gt;1GR-FE ENGINE CONTROL&gt;THROTTLE BODY&gt;REMOVAL<span class="invisible">200908,201308,_51,_056124,_0273863,RM100000000XNQ0,</span><span class="invisible">201308,999999,_51,_056124,_0273863,RM100000000XNQ1,</span></a>)
</p>
</div>
<br>
<div class="floatClear"></div>
</div>
<div class="floatClear"></div>
</div>
</div>
</div>
</div>
<div id="footer"></div>
</div>
</div>
</body>
</html>
