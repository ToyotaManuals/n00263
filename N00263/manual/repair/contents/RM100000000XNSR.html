<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<link rel="stylesheet" type="text/css" href="../../../system/css/global.css">
<link rel="stylesheet" type="text/css" href="../css/contents.css">
<link rel="stylesheet" type="text/css" href="../../../system/css/facebox.css">
<link rel="stylesheet" type="text/css" href="../css/print.css" id="print_css">
<script type="text/javascript" src="../../../system/js/util.js?cntl=Contents" charset="UTF-8"></script>
<script type="text/javascript" src="../../../system/js/dict_const.js" charset="UTF-8"></script>
<script type="text/javascript" src="../../../system/js/dict_message.js" charset="UTF-8"></script>
<script type="text/javascript" src="../../../system/js/use.js" charset="UTF-8"></script>
<script type="text/javascript" src="../../../system/js/contents.js" charset="UTF-8"></script>
<title>Toyota Service Information</title>
</head>
<body>
<div id="wrapper">
<div id="header"></div>
<div id="body">
<div id="contents">
<div id="contentsBody" class="fontEn">
<div class="globalInfo">
<span class="globalPubNo">RM1440E</span>
<span class="globalServcat">_51</span>
<span class="globalServcatName">Engine / Hybrid System</span>
<span class="globalSection">_056124</span>
<span class="globalSectionName">1GR-FE ENGINE CONTROL</span>
<span class="globalTitle">_0273876</span>
<span class="globalTitleName">SFI SYSTEM</span>
<span class="globalCategory">C</span>
</div>
<h1>1GR-FE ENGINE CONTROL&nbsp;&nbsp;SFI SYSTEM&nbsp;&nbsp;P2119&nbsp;&nbsp;Throttle Actuator Control Throttle Body Range / Performance&nbsp;&nbsp;</h1>
<br>
<div id="RM100000000XNSR_01" class="category no03">
<h2>DESCRIPTION</h2>
<div class="content5">
<p>The Electronic Throttle Control System (ETCS) is composed of the throttle actuator, throttle position sensor, accelerator pedal position sensor and ECM. The ECM operates the throttle actuator to regulate the throttle valve in response to driver inputs. The throttle position sensor detects the opening angle of the throttle valve, and provides the ECM with feedback so that the throttle valve can be appropriately controlled by the ECM.
</p>
<table summary="">
<colgroup>
<col style="width:12%">
<col style="width:44%">
<col style="width:43%">
</colgroup>
<thead>
<tr>
<th class="alcenter">DTC No.
</th>
<th class="alcenter">DTC Detection Condition
</th>
<th class="alcenter">Trouble Area
</th>
</tr>
</thead>
<tbody>
<tr>
<td class="alcenter">P2119
</td>
<td>Throttle valve opening angle continues to vary greatly from the target opening angle (1 trip detection logic).
</td>
<td><div class="list1">
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>ETCS (Electronic Throttle Control System)
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>ECM
</p>
</div>
</div>
</div>
<br>
</td>
</tr>
</tbody>
</table>
<br>
</div>
</div>
<div id="RM100000000XNSR_02" class="category no03">
<h2>MONITOR DESCRIPTION</h2>
<div class="content5">
<p>The ECM determines the actual opening angle of the throttle valve from the throttle position sensor signal. The actual opening angle is compared to the target opening angle calculated by the ECM. If the difference between these two values is outside the standard range, the ECM interprets this as a malfunction in the ETCS. The ECM then illuminates the MIL and stores the DTC.
</p>
<p>If the malfunction is not repaired successfully, the DTC is stored when the accelerator pedal is quickly released (to close the throttle valve) after the engine speed reaches 5000 rpm by the accelerator pedal being fully depressed (fully opening the throttle valve).
</p>
</div>
</div>
<div id="RM100000000XNSR_06" class="category no03">
<h2>FAIL-SAFE</h2>
<div class="content5">
<p>When this DTC or other DTCs relating to ETCS (Electronic Throttle Control System) malfunctions are set, the ECM enters fail-safe mode. During fail-safe mode, the ECM cuts the current to the throttle actuator, and the throttle valve is returned to a 7° opening angle by the return spring. The ECM then adjusts the engine output by controlling the fuel injection (intermittent fuel-cut) and ignition timing in accordance with the accelerator pedal position to allow the vehicle to continue at a minimal speed. If the accelerator pedal is depressed firmly and gently, the vehicle can be driven slowly.
</p>
<p>The ECM continues operating in fail-safe mode until a pass condition is detected and the ignition switch is turned off.
</p>
</div>
</div>
<div id="RM100000000XNSR_07" class="category no03">
<h2>WIRING DIAGRAM</h2>
<div class="content5">
<p>Refer to DTC P2102 (See page <a class="mlink" href="javascript:void(0)">Engine / Hybrid System&gt;1GR-FE ENGINE CONTROL&gt;SFI SYSTEM&gt;P2102<span class="invisible">200908,999999,_51,_056124,_0273876,RM100000000XNSO_07,P2102</span></a>).
</p>
</div>
</div>
<div id="RM100000000XNSR_08" class="category no10">
<h2>CAUTION / NOTICE / HINT</h2>
<div class="content5">
<dl class="atten4">
<dt class="atten4">HINT:</dt>
<dd class="atten4">
<p>Read freeze frame data using the intelligent tester. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, if the air-fuel ratio was lean or rich, and other data from the time the malfunction occurred.
</p>
</dd>
</dl>
<br>
</div>
</div>
<div id="RM100000000XNSR_09" class="category no01">
<h2>PROCEDURE</h2>
<div class="testgrp" id="RM100000000XNSR_09_0001">
<div class="testtitle"><span class="titleText">1.CHECK FOR ANY OTHER DTCS OUTPUT (IN ADDITION TO DTC P2119)</span></div>
<div class="content6">
<div class="test1">
<div class="test1Item">
<div class="test1Head">a.</div>
<div class="test1Body">
<p>Connect the intelligent tester to the DLC3.
</p>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">b.</div>
<div class="test1Body">
<p>Turn the ignition switch to ON.
</p>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">c.</div>
<div class="test1Body">
<p>Turn the intelligent tester on.
</p>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">d.</div>
<div class="test1Body">
<p>Enter the following menus: Powertrain / Engine and ECT / Trouble Code.
</p>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">e.</div>
<div class="test1Body">
<p>Read DTCs.
</p>
<table summary="">
<caption>Result</caption>
<colgroup>
<col style="width:50%">
<col style="width:50%">
</colgroup>
<thead>
<tr>
<th class="alcenter">Display (DTC Output)
</th>
<th class="alcenter">Proceed to
</th>
</tr>
</thead>
<tbody>
<tr>
<td class="alcenter">P2119
</td>
<td class="alcenter">A
</td>
</tr>
<tr>
<td class="alcenter">P2119 and other DTCs
</td>
<td class="alcenter">B
</td>
</tr>
</tbody>
</table>
<br>
<dl class="atten4">
<dt class="atten4">HINT:</dt>
<dd class="atten4">
<p>If any DTCs other than P2119 are output, troubleshoot those DTCs first.
</p>
</dd>
</dl>
<br>
</div>
</div>
</div>
<br>
</div>
<br>
<div class="judgeValueNext"><p>A
</p></div>
<div class="nextAction"><span class="titleText">2.INSPECT THROTTLE BODY WITH MOTOR ASSEMBLY (RESISTANCE OF THROTTLE ACTUATOR)</span></div>
<div class="judgeValueEnd"><p>B
</p></div>
<div class="endActBox">
<div class="endAction"><span class="titleText">GO TO DTC CHART</span></div>
<div class="content6">
<p>(See page<a class="mlink" href="javascript:void(0)">Engine / Hybrid System&gt;1GR-FE ENGINE CONTROL&gt;SFI SYSTEM&gt;DIAGNOSTIC TROUBLE CODE CHART<span class="invisible">200908,201308,_51,_056124,_0273876,RM100000000XNRJ,</span><span class="invisible">201308,201408,_51,_056124,_0273876,RM100000000XNRK,</span><span class="invisible">201408,201508,_51,_056124,_0273876,RM100000000XNRL,</span><span class="invisible">201508,999999,_51,_056124,_0273876,RM100000000XNRM,</span></a>)
</p>
</div>
<br>
<div class="floatClear"></div>
</div>
<div class="floatClear"></div>
</div>
<div class="testgrp" id="RM100000000XNSR_09_0006">
<div class="testtitle"><span class="titleText">2.INSPECT THROTTLE BODY WITH MOTOR ASSEMBLY (RESISTANCE OF THROTTLE ACTUATOR)</span></div>
<div class="content6">
<div class="test1">
<div class="test1Item">
<div class="test1Head">a.</div>
<div class="test1Body">
<p>Inspect the throttle body with motor assembly (See page <a class="mlink" href="javascript:void(0)">Engine / Hybrid System&gt;1GR-FE ENGINE CONTROL&gt;THROTTLE BODY&gt;INSPECTION<span class="invisible">200908,999999,_51,_056124,_0273863,RM100000000XNPX_01_0001,</span></a>).
</p>
</div>
</div>
</div>
<br>
</div>
<br>
<div class="judgeValueNext"><p>OK
</p></div>
<div class="nextAction"><span class="titleText">3.REPLACE ECM</span></div>
<div class="judgeValueEnd"><p>NG
</p></div>
<div class="endActBox">
<div class="endAction"><span class="titleText">REPLACE THROTTLE BODY WITH MOTOR ASSEMBLY</span></div>
<div class="content6">
<p>(See page<a class="mlink" href="javascript:void(0)">Engine / Hybrid System&gt;1GR-FE ENGINE CONTROL&gt;THROTTLE BODY&gt;REMOVAL<span class="invisible">200908,201308,_51,_056124,_0273863,RM100000000XNQ0,</span><span class="invisible">201308,999999,_51,_056124,_0273863,RM100000000XNQ1,</span></a>)
</p>
</div>
<br>
<div class="floatClear"></div>
</div>
<div class="floatClear"></div>
</div>
<div class="testgrp" id="RM100000000XNSR_09_0007">
<div class="testtitle"><span class="titleText">3.REPLACE ECM</span></div>
<div class="content6">
<div class="test1">
<div class="test1Item">
<div class="test1Head">a.</div>
<div class="test1Body">
<p>Replace the ECM (See page <a class="mlink" href="javascript:void(0)">Engine / Hybrid System&gt;1GR-FE ENGINE CONTROL&gt;ECM&gt;REMOVAL<span class="invisible">200908,999999,_51,_056124,_0273864,RM100000000XNQ5,</span></a>).
</p>
</div>
</div>
</div>
<br>
</div>
<br>
<div class="judgeValueNext"><p>NEXT
</p></div>
<div class="nextAction"><span class="titleText">4.CHECK WHETHER DTC OUTPUT RECURS (DTC P2119)</span></div>
<div class="floatClear"></div>
</div>
<div class="testgrp" id="RM100000000XNSR_09_0002">
<div class="testtitle"><span class="titleText">4.CHECK WHETHER DTC OUTPUT RECURS (DTC P2119)</span></div>
<div class="content6">
<div class="test1">
<div class="test1Item">
<div class="test1Head">a.</div>
<div class="test1Body">
<p>Connect the intelligent tester to the DLC3.
</p>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">b.</div>
<div class="test1Body">
<p>Turn the ignition switch to ON.
</p>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">c.</div>
<div class="test1Body">
<p>Turn the intelligent tester on.
</p>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">d.</div>
<div class="test1Body">
<p>Clear DTCs (See page <a class="mlink" href="javascript:void(0)">Engine / Hybrid System&gt;1GR-FE ENGINE CONTROL&gt;SFI SYSTEM&gt;DTC CHECK / CLEAR<span class="invisible">200908,999999,_51,_056124,_0273876,RM100000000XNT0,</span></a>).
</p>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">e.</div>
<div class="test1Body">
<p>Allow the engine to idle for 15 seconds or more.
</p>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">f.</div>
<div class="test1Body">
<p>Fully depress and release the accelerator pedal several times quickly.
</p>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">g.</div>
<div class="test1Body">
<p>Enter the following menus: Powertrain / Engine and ECT / Trouble Codes.
</p>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">h.</div>
<div class="test1Body">
<p>Read DTCs.
</p>
<dl class="spec">
<dt class="spec">OK:</dt>
<dd class="spec"><p>No DTC is output.
</p>
</dd>
</dl>
<br>
<dl class="atten4">
<dt class="atten4">HINT:</dt>
<dd class="atten4">
<p>The output voltage of the throttle position sensor can be checked using the intelligent tester. Variations in the output voltage indicate that the throttle actuator is in operation. To check the output voltage using the intelligent tester, enter the following menus: Powertrain / Engine and ECT / Data List / All Data / Throttle Position No. 1.
</p>
</dd>
</dl>
<br>
</div>
</div>
</div>
<br>
</div>
<br>
<div class="judgeValueEnd"><p>OK
</p></div>
<div class="endActBox">
<div class="endAction"><span class="titleText">END</span></div>
<div class="content6">
</div>
<br>
<div class="floatClear"></div>
</div>
<div class="judgeValueEnd"><p>NG
</p></div>
<div class="endActBox">
<div class="endAction"><span class="titleText">REPLACE THROTTLE BODY WITH MOTOR ASSEMBLY</span></div>
<div class="content6">
<p>(See page<a class="mlink" href="javascript:void(0)">Engine / Hybrid System&gt;1GR-FE ENGINE CONTROL&gt;THROTTLE BODY&gt;REMOVAL<span class="invisible">200908,201308,_51,_056124,_0273863,RM100000000XNQ0,</span><span class="invisible">201308,999999,_51,_056124,_0273863,RM100000000XNQ1,</span></a>)
</p>
</div>
<br>
<div class="floatClear"></div>
</div>
<div class="floatClear"></div>
</div>
</div>
</div>
</div>
<div id="footer"></div>
</div>
</div>
</body>
</html>
